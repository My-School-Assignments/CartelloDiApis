# Cartello Di Apis
---
Management application for Drug Cartel business.

Demo Accounts:
- Manager:
    Email: manager@app.com
    PWD: pwd123
- Employees -> Create when logged in as Manager
    -> Default PWD: Default.123
- Customers -> Can be created on LogIn page 
    -> Emails are NOT validated so anything in form a@b.c will work

The link to the working app: http://cartellodiapis.azurewebsites.net/

© Dominik Hornak, Richard Halcin (2018)

---
#### NOTE: THIS IS NOT A REAL DRUG CARTEL OPERATION. THE PROJECT WAS MADE PURELY FOR SCHOOL ASSIGNMENT AS A JOKE!