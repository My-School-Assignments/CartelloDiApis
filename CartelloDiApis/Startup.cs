﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using CartelloDiApis.App_Start;
using CartelloDiApis.Models;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CartelloDiApis.Startup))]
namespace CartelloDiApis
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesAndUsers();

            GlobalConfiguration.Configuration
                .UseSqlServerStorage("DefaultConnection");
            
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                AuthorizationFilters = Enumerable.Empty<IAuthorizationFilter>()
            });
            app.UseHangfireServer();
        }

        private void createRolesAndUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            if (context.AccountantModels.Find("DefaultAccountant") == null)
            {
                var acc = new AccountantModel();
                acc.AcountantName = "DefaultAccountant";
                acc.SafeMoney = 0;
                context.AccountantModels.Add(acc);
                context.SaveChanges();
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("Manager"))
            {
                // first we create Admin role    
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Manager";
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website
                var user = new ApplicationUser();
                user.UserName = "manager@app.com";
                user.Email = "manager@app.com";

                string userPWD = "pwd123";

                var chkUser = UserManager.Create(user, userPWD);

                //Add default User to Role Admin    
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Manager");
                }
            }

            // creating Creating Employee role     
            if (!roleManager.RoleExists("Employee"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Employee";
                roleManager.Create(role);
            }

            // creating Creating Customer role     
            if (!roleManager.RoleExists("Customer"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Customer";
                roleManager.Create(role);
            }

            if (context.WarehouseModels.Find("InputWHouse1") == null)
            {
                var whouse = new WarehouseModel();
                whouse.Name = "InputWHouse1";
                whouse.Capacity = 3500;
                whouse.FreeCapacity = 3500;
                whouse.Type = WarehouseType.Input;
                context.WarehouseModels.Add(whouse);
                context.SaveChanges();
            }

            if (context.WarehouseModels.Find("OutputWHouse1") == null)
            {
                var whouse = new WarehouseModel();
                whouse.Name = "OutputWHouse1";
                whouse.Capacity = 3500;
                whouse.FreeCapacity = 3500;
                whouse.Type = WarehouseType.Output;
                context.WarehouseModels.Add(whouse);
                context.SaveChanges();
            }

            if (context.ProductionLineModels.Find("MarihuanaLine1") == null)
            {
                var line = new ProductionLineModel();
                line.LineName = "MarihuanaLine1";
                line.MainSwitch = Switch.Running;
                line.LineState = State.Stopped;
                line.CheckingQualityPercent = 0;
                line.GetMaterialPercent = 0;
                line.ProducingPercent = 0;
                line.StoringPercent = 0;
                context.ProductionLineModels.Add(line);
                context.SaveChanges();
            }

            if (context.ProductionLineModels.Find("MarihuanaLine2") == null)
            {
                var line = new ProductionLineModel();
                line.LineName = "MarihuanaLine2";
                line.MainSwitch = Switch.Running;
                line.LineState = State.Stopped;
                line.CheckingQualityPercent = 0;
                line.GetMaterialPercent = 0;
                line.ProducingPercent = 0;
                line.StoringPercent = 0;
                context.ProductionLineModels.Add(line);
                context.SaveChanges();
            }

            if (context.ProductionLineModels.Find("CocaineLine1") == null)
            {
                var line = new ProductionLineModel();
                line.LineName = "CocaineLine1";
                line.MainSwitch = Switch.Running;
                line.LineState = State.Stopped;
                line.CheckingQualityPercent = 0;
                line.GetMaterialPercent = 0;
                line.ProducingPercent = 0;
                line.StoringPercent = 0;
                context.ProductionLineModels.Add(line);
                context.SaveChanges();
            }

            if (context.ProductionLineModels.Find("CocaineLine2") == null)
            {
                var line = new ProductionLineModel();
                line.LineName = "CocaineLine2";
                line.MainSwitch = Switch.Running;
                line.LineState = State.Stopped;
                line.CheckingQualityPercent = 0;
                line.GetMaterialPercent = 0;
                line.ProducingPercent = 0;
                line.StoringPercent = 0;
                context.ProductionLineModels.Add(line);
                context.SaveChanges();
            }

            if (context.ProductionLineModels.Find("MethLine1") == null)
            {
                var line = new ProductionLineModel();
                line.LineName = "MethLine1";
                line.MainSwitch = Switch.Running;
                line.LineState = State.Stopped;
                line.CheckingQualityPercent = 0;
                line.GetMaterialPercent = 0;
                line.ProducingPercent = 0;
                line.StoringPercent = 0;
                context.ProductionLineModels.Add(line);
                context.SaveChanges();
            }

            if (context.ProductionLineModels.Find("MethLine2") == null)
            {
                var line = new ProductionLineModel();
                line.LineName = "MethLine2";
                line.MainSwitch = Switch.Running;
                line.LineState = State.Stopped;
                line.CheckingQualityPercent = 0;
                line.GetMaterialPercent = 0;
                line.ProducingPercent = 0;
                line.StoringPercent = 0;
                context.ProductionLineModels.Add(line);
                context.SaveChanges();
            }

            var it = context.InputWarehouseModels.Where(p => p.ProductName == "Marihuana Seed").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Marihuana Seed";
                item.PricePerItem = 150;
                item.ProductCategory = Category.MarihuanaMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Soil").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Soil";
                item.PricePerItem = 300;
                item.ProductCategory = Category.MarihuanaMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Fertilizer").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Fertilizer";
                item.PricePerItem = 500;
                item.ProductCategory = Category.MarihuanaMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Coca Leaves").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Coca Leaves";
                item.PricePerItem = 600;
                item.ProductCategory = Category.CocainMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Solvent").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Solvent";
                item.PricePerItem = 1300;
                item.ProductCategory = Category.CocainMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Purifier").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Purifier";
                item.PricePerItem = 400;
                item.ProductCategory = Category.CocainMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Pills").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Pills";
                item.PricePerItem = 1500;
                item.ProductCategory = Category.MethMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Phosphorus").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Phosphorus";
                item.PricePerItem = 2000;
                item.ProductCategory = Category.MethMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }

            it = context.InputWarehouseModels.Where(p => p.ProductName == "Lye Solution").ToList();
            if (it.Count == 0)
            {
                var item = new InputWarehouseModel();
                item.Quantity = 300;
                item.ProductName = "Lye Solution";
                item.PricePerItem = 1000;
                item.ProductCategory = Category.MethMaterial;
                item.SpaceRequiredPerItem = 1;
                item.WarehouseAcceptanceDate = DateTime.Now;
                item.WarehouseModelName = "InputWHouse1";
                context.InputWarehouseModels.Add(item);
                context.SaveChanges();

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = -item.Quantity * item.PricePerItem;
                trans.Date = DateTime.Today;
                trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                context.TransactionModels.Add(trans);
                context.SaveChanges();

                var acc = context.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                context.AccountantModels.AddOrUpdate(acc);
                context.SaveChanges();
            }
        }
    }
}
