namespace CartelloDiApis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountantModels",
                c => new
                    {
                        AcountantName = c.String(nullable: false, maxLength: 128),
                        SafeMoney = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.AcountantName);
            
            CreateTable(
                "dbo.TransactionModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        Reason = c.String(),
                        Date = c.DateTime(nullable: false),
                        AccountantModelAcountantName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccountantModels", t => t.AccountantModelAcountantName)
                .Index(t => t.AccountantModelAcountantName);
            
            CreateTable(
                "dbo.EmployeeModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        PostalCode = c.String(),
                        HourlyPay = c.Double(nullable: false),
                        HoursThisPeriod = c.Double(nullable: false),
                        BitcoinAddress = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PaycheckModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PaycheckDate = c.DateTime(nullable: false),
                        Hours = c.Double(nullable: false),
                        HourlyPay = c.Double(nullable: false),
                        Tax = c.Double(nullable: false),
                        EmployeeModelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmployeeModels", t => t.EmployeeModelID, cascadeDelete: true)
                .Index(t => t.EmployeeModelID);
            
            CreateTable(
                "dbo.InputWarehouseModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        PricePerItem = c.Double(nullable: false),
                        SpaceRequiredPerItem = c.Int(nullable: false),
                        WarehouseAcceptanceDate = c.DateTime(nullable: false),
                        ProductCategory = c.Int(nullable: false),
                        WarehouseModelName = c.Int(nullable: false),
                        WarehouseModel_Name = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.WarehouseModels", t => t.WarehouseModel_Name)
                .Index(t => t.WarehouseModel_Name);
            
            CreateTable(
                "dbo.WarehouseModels",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        Capacity = c.Int(nullable: false),
                        FreeCapacity = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.ItemModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        PricePerUnit = c.Double(nullable: false),
                        OrderModelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.OrderModels", t => t.OrderModelID, cascadeDelete: true)
                .Index(t => t.OrderModelID);
            
            CreateTable(
                "dbo.OrderModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CustomerAddress1 = c.String(),
                        CustomerAddress2 = c.String(),
                        CustomerPostalCode = c.String(),
                        CustomerName = c.String(),
                        Companyddress1 = c.String(),
                        CompanyAddress2 = c.String(),
                        CompanyPostalCode = c.String(),
                        CompanyName = c.String(),
                        OrderStatus = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OutputWarehouseModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        PricePerItem = c.Double(nullable: false),
                        SpaceRequiredPerItem = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        ProductCategory = c.Int(nullable: false),
                        WarehouseModelName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.WarehouseModels", t => t.WarehouseModelName)
                .Index(t => t.WarehouseModelName);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.OutputWarehouseModels", "WarehouseModelName", "dbo.WarehouseModels");
            DropForeignKey("dbo.ItemModels", "OrderModelID", "dbo.OrderModels");
            DropForeignKey("dbo.InputWarehouseModels", "WarehouseModel_Name", "dbo.WarehouseModels");
            DropForeignKey("dbo.PaycheckModels", "EmployeeModelID", "dbo.EmployeeModels");
            DropForeignKey("dbo.TransactionModels", "AccountantModelAcountantName", "dbo.AccountantModels");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.OutputWarehouseModels", new[] { "WarehouseModelName" });
            DropIndex("dbo.ItemModels", new[] { "OrderModelID" });
            DropIndex("dbo.InputWarehouseModels", new[] { "WarehouseModel_Name" });
            DropIndex("dbo.PaycheckModels", new[] { "EmployeeModelID" });
            DropIndex("dbo.TransactionModels", new[] { "AccountantModelAcountantName" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.OutputWarehouseModels");
            DropTable("dbo.OrderModels");
            DropTable("dbo.ItemModels");
            DropTable("dbo.WarehouseModels");
            DropTable("dbo.InputWarehouseModels");
            DropTable("dbo.PaycheckModels");
            DropTable("dbo.EmployeeModels");
            DropTable("dbo.TransactionModels");
            DropTable("dbo.AccountantModels");
        }
    }
}
