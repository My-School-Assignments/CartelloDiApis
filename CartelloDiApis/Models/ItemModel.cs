﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class ItemModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public double PricePerUnit { get; set; }
        public int OrderModelID { get; set; }

        public virtual OrderModel OrderModel { get; set; }
    }
}