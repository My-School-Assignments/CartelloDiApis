﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class AccountantModel
    {
        [Key]
        public string AcountantName { get; set; }
        public double SafeMoney { get; set; }

        public virtual ICollection<TransactionModel> Transactions { get; set; }
    }
}