﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class PaycheckModel
    {
        public int ID { get; set; }
        public DateTime PaycheckDate { get; set; }
        public double Hours { get; set; }
        public double HourlyPay { get; set; }
        public double Tax { get; set; }
        public int EmployeeModelID { get; set; }

        public virtual EmployeeModel EmployeeModel { get; set; }
    }
}