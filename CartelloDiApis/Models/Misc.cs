﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public enum Category
    {
        MarihuanaMaterial,
        CocainMaterial,
        MethMaterial,
        Product
    }

    public enum State
    {
        Stopped,
        RunningGettingMaterial,
        RunningProducing,
        RunningQuality,
        RunningStoring
    }

    public enum Switch
    {
        Stopped,
        Running
    }

    public enum WarehouseType
    {
        Input,
        Output
    }

    public enum Status
    {
        New,
        Accepted,
        Declined,
        InFactory,
        Finished
    }
}