﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class HomeViewModel
    {
        public IList<TransactionModel> Transactions { get; set; }
        public IList<ProductionLineModel> ProductionLines { get; set; }
        public IList<WarehouseModel> Warehouses { get; set; }
        public double Money { get; set; }
    }
}