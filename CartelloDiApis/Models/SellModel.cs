﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class SellModel
    {
        public string ProductName { get; set; }
        public int Amount { get; set; }
    }
}