﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class TransactionModel
    {
        public int ID { get; set; }
        public double Amount { get; set; }
        public string Reason { get; set; }
        public DateTime Date { get; set; }
        public string AccountantModelAcountantName { get; set; }

        public virtual AccountantModel AccountantModel { get; set; }
    }
}