﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class LineQueueModel
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public int Amount { get; set; }

        public string ProductionLineModelLineName { get; set; }
        public virtual ProductionLineModel ProductionLineModel { get; set; }
    }
}