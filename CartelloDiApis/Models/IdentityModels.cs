﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CartelloDiApis.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.InputWarehouseModel> InputWarehouseModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.OutputWarehouseModel> OutputWarehouseModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.WarehouseModel> WarehouseModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.AccountantModel> AccountantModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.TransactionModel> TransactionModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.PaycheckModel> PaycheckModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.EmployeeModel> EmployeeModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.ItemModel> ItemModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.OrderModel> OrderModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.LineQueueModel> LineQueueModels { get; set; }

        public System.Data.Entity.DbSet<CartelloDiApis.Models.ProductionLineModel> ProductionLineModels { get; set; }
    }
}