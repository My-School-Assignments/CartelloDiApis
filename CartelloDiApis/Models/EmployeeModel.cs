﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class EmployeeModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public double HourlyPay { get; set; }
        public double HoursThisPeriod { get; set; }
        public string BitcoinAddress { get; set; } // Not a Bank Account because of nature of our operation of course....
        public virtual ICollection<PaycheckModel> Paychecks { get; set; }
    }
}