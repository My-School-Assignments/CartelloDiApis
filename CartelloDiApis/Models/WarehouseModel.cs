﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class WarehouseModel
    {
        [Key]
        public string Name { get; set; }
        public int Capacity { get; set; }
        public int FreeCapacity { get; set; }
        public WarehouseType Type { get; set; }

        public virtual ICollection<IWarehouse> Items { get; set; }
    }
}