﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class ProductionLineModel
    {
        [Key]
        public string LineName { get; set; }
        public Switch MainSwitch { get; set; }
        public State LineState { get; set; }
        public int GetMaterialPercent { get; set; }
        public int ProducingPercent { get; set; }
        public int CheckingQualityPercent { get; set; }
        public int StoringPercent { get; set; }

        public virtual ICollection<LineQueueModel> Queue { get; set; }
    }
}