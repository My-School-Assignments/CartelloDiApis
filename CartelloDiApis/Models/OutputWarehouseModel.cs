﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class OutputWarehouseModel : IWarehouse
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double PricePerItem { get; set; }
        public int SpaceRequiredPerItem { get; set; }
        public DateTime CreationDate { get; set; }
        public Category ProductCategory { get; set; }
        public string WarehouseModelName { get; set; }

        public virtual WarehouseModel WarehouseModel { get; set; }
    }
}