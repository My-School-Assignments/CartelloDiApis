﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CartelloDiApis.Models
{
    public class OrderModel
    {
        public int ID { get; set; }
        public string CustomerAddress1 { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerPostalCode { get; set; }
        public string CustomerName { get; set; }
        public string Companyddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyName { get; set; }
        public Status OrderStatus { get; set; }
        public DateTime Date { get; set; }
        public string Email { get; set; }

        public virtual ICollection<ItemModel> Items { get; set; }
    }
}