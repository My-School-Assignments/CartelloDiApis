﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CartelloDiApis.Controllers
{
    // Controller for DashBoard page
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                var viewModel = new HomeViewModel();
                viewModel.Transactions = new List<TransactionModel>();
                viewModel.ProductionLines = new List<ProductionLineModel>();
                viewModel.Warehouses = new List<WarehouseModel>();

                var trans = db.TransactionModels;
                foreach (var tran in trans)
                {
                    viewModel.Transactions.Add(tran);
                }

                var lines = db.ProductionLineModels;
                foreach (var line in lines)
                {
                    viewModel.ProductionLines.Add(line);
                }

                var whouses = db.WarehouseModels;
                foreach (var whouse in whouses)
                {
                    viewModel.Warehouses.Add(whouse);
                }

                viewModel.Money = db.AccountantModels.Find("DefaultAccountant").SafeMoney;

                return View(viewModel);
            }
            else if (User.Identity.IsAuthenticated && User.IsInRole("Customer"))
            {
                return RedirectToAction("MyOrders", "Order");
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }
    }
}