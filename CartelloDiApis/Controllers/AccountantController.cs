﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;

namespace CartelloDiApis.Controllers
{
    public class AccountantController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Accountant/PayEmployees
        public async Task<ActionResult> PayEmployees()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Manager"))
            {
                var acc = db.AccountantModels.Find("DefaultAccountant");
                var employees = new List<EmployeeModel>();

                foreach (var emp in db.EmployeeModels)
                {
                    employees.Add(emp);
                }

                foreach (var emp in employees)
                {
                    var paycheck = new PaycheckModel();
                    paycheck.HourlyPay = emp.HourlyPay;
                    paycheck.Hours = emp.HoursThisPeriod;
                    paycheck.PaycheckDate = DateTime.Today;
                    paycheck.EmployeeModelID = emp.ID;
                    paycheck.Tax = 0;

                    var trans = new TransactionModel();
                    trans.AccountantModelAcountantName = "DefaultAccountant";
                    trans.Amount = -paycheck.HourlyPay * paycheck.Hours;
                    trans.Date = DateTime.Today;
                    trans.Reason = "Paycheck to: " + emp.Name;

                    db.TransactionModels.Add(trans);

                    acc.SafeMoney -= paycheck.HourlyPay * paycheck.Hours;

                    db.PaycheckModels.Add(paycheck);
                    emp.HoursThisPeriod = 0;

                    db.EmployeeModels.AddOrUpdate(emp);
                }

                db.AccountantModels.AddOrUpdate(acc);

                await db.SaveChangesAsync();

                return RedirectToAction("PayEmployeesCompleted");
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        public ActionResult PayEmployeesCompleted()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
