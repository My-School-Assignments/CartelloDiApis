﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;
using Hangfire;

namespace CartelloDiApis.Controllers
{
    public class ProductionLineController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductionLine/Marihuana
        public ActionResult Marihuana()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                return View(db.ProductionLineModels.Where(p => p.LineName.Contains("Marihuana")).ToList());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: ProductionLine/Cocaine
        public ActionResult Cocaine()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                return View(db.ProductionLineModels.Where(p => p.LineName.Contains("Cocaine")).ToList());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: ProductionLine/Meth
        public ActionResult Meth()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                return View(db.ProductionLineModels.Where(p => p.LineName.Contains("Meth")).ToList());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        public  ActionResult StartAll()
        {
            var lines = db.ProductionLineModels.ToList();

            foreach (var line in lines)
            {
                line.MainSwitch = Switch.Running;
                db.ProductionLineModels.AddOrUpdate(line);
                db.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult StopAll()
        {
            var lines = db.ProductionLineModels.ToList();

            foreach (var line in lines)
            {
                line.MainSwitch = Switch.Stopped;
                db.ProductionLineModels.AddOrUpdate(line);
                db.SaveChanges();
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public async Task<ActionResult> StartMarihuana()
        {
            var lines = db.ProductionLineModels.Where(p => p.LineName.Contains("Marihuana"));
            if (lines == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            foreach (var line in lines)
            {
                if (line.Queue == null || line.Queue.Count == 0)
                {
                    line.LineState = State.Stopped;
                    line.CheckingQualityPercent = 0;
                    line.GetMaterialPercent = 0;
                    line.ProducingPercent = 0;
                    line.StoringPercent = 0;

                    db.ProductionLineModels.AddOrUpdate(line);
                    await db.SaveChangesAsync();
                }
                else
                {
                    BackgroundJob.Enqueue(() => 
                        RunLine(
                            line.LineName, 
                            Url.Action("Take", "InputWarehouse", null, Request.Url.Scheme), 
                            Url.Action("Store", "OutputWarehouse", null, Request.Url.Scheme),
                            Url.Action("StartMarihuana", "ProductionLine", null, Request.Url.Scheme)));
                }
            }

            Thread.Sleep(110);

            return RedirectToAction("Index", "Home");
        }

        public void RunLine(string lineName, string takeUrl, string storeUrl, string restartUrl)
        {
            using (var dbo = new ApplicationDbContext())
            {
                var line = dbo.ProductionLineModels.Find(lineName);

                if (line.LineState != State.Stopped) return;

                while (line.MainSwitch == Switch.Stopped)
                {
                    Thread.Sleep(5000);
                    line = dbo.ProductionLineModels.Find(lineName);
                }

                line = dbo.ProductionLineModels.Find(lineName);

                var order = line.Queue.First();
                dbo.LineQueueModels.Remove(order);
                dbo.SaveChanges();

                // Get Material

                #region Get

                line.LineState = State.RunningGettingMaterial;
                dbo.ProductionLineModels.AddOrUpdate(line);
                dbo.SaveChanges();

                if (line.LineName.Contains("Marihuana"))
                {
                    string data = "ProductName=Marihuana Seed&Amount=1";
                    HttpWebRequest httpRequest =
                        (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    var result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }

                    data = "ProductName=Soil&Amount=1";
                    httpRequest = (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }

                    data = "ProductName=Fertilizer&Amount=1";
                    httpRequest = (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }
                }
                if (line.LineName.Contains("Cocaine"))
                {
                    string data = "ProductName=Coca Leaves&Amount=1";
                    HttpWebRequest httpRequest =
                        (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    var result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }

                    data = "ProductName=Solvent&Amount=1";
                    httpRequest = (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }

                    data = "ProductName=Purifier&Amount=1";
                    httpRequest = (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }
                }
                if (line.LineName.Contains("Meth"))
                {
                    string data = "ProductName=Pills&Amount=1";
                    HttpWebRequest httpRequest =
                        (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    var result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }

                    data = "ProductName=Phosphorus&Amount=1";
                    httpRequest = (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }

                    data = "ProductName=Lye Solution&Amount=1";
                    httpRequest = (HttpWebRequest)WebRequest.Create(takeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }
                }

                var stopWatch = DateTime.Now;
                while ((DateTime.Now - stopWatch).TotalSeconds <= 30)
                {
                    var p = ((DateTime.Now - stopWatch).TotalSeconds / 30.0d) * 100.0d;
                    line.GetMaterialPercent = (int)p;
                    dbo.ProductionLineModels.AddOrUpdate(line);
                    dbo.SaveChanges();

                    Thread.Sleep(1000);
                }

                line.GetMaterialPercent = 100;
                line.LineState = State.RunningProducing;
                dbo.ProductionLineModels.AddOrUpdate(line);
                dbo.SaveChanges();

                // Get Material Finished - Check if Main switch stopped
                line = dbo.ProductionLineModels.Find(lineName);
                while (line.MainSwitch == Switch.Stopped)
                {
                    Thread.Sleep(5000);
                    line = dbo.ProductionLineModels.Find(lineName);
                }

                #endregion

                // Run Production

                #region Production

                stopWatch = DateTime.Now;
                while ((DateTime.Now - stopWatch).TotalSeconds <= 60)
                {
                    var p = ((DateTime.Now - stopWatch).TotalSeconds / 60.0d) * 100.0d;
                    line.ProducingPercent = (int)p;
                    dbo.ProductionLineModels.AddOrUpdate(line);
                    dbo.SaveChanges();

                    Thread.Sleep(1000);
                }

                line.ProducingPercent = 100;
                line.LineState = State.RunningQuality;
                dbo.ProductionLineModels.AddOrUpdate(line);
                dbo.SaveChanges();

                // Production Finished - Check if Main switch stopped
                line = dbo.ProductionLineModels.Find(lineName);
                while (line.MainSwitch == Switch.Stopped)
                {
                    Thread.Sleep(5000);
                    line = dbo.ProductionLineModels.Find(lineName);
                }

                #endregion

                // Quality Check

                #region Quality

                stopWatch = DateTime.Now;
                while ((DateTime.Now - stopWatch).TotalSeconds <= 30)
                {
                    var p = ((DateTime.Now - stopWatch).TotalSeconds / 30.0d) * 100.0d;
                    line.CheckingQualityPercent = (int)p;
                    dbo.ProductionLineModels.AddOrUpdate(line);
                    dbo.SaveChanges();

                    Thread.Sleep(1000);
                }

                line.CheckingQualityPercent = 100;
                line.LineState = State.RunningStoring;
                dbo.ProductionLineModels.AddOrUpdate(line);
                dbo.SaveChanges();

                // Quality Finished - Check if Main switch stopped
                line = dbo.ProductionLineModels.Find(lineName);
                while (line.MainSwitch == Switch.Stopped)
                {
                    Thread.Sleep(5000);
                    line = dbo.ProductionLineModels.Find(lineName);
                }

                #endregion

                // Storing

                #region Store

                stopWatch = DateTime.Now;
                while ((DateTime.Now - stopWatch).TotalSeconds <= 30)
                {
                    var p = ((DateTime.Now - stopWatch).TotalSeconds / 30.0d) * 100.0d;
                    line.StoringPercent = (int)p;
                    dbo.ProductionLineModels.AddOrUpdate(line);
                    dbo.SaveChanges();

                    Thread.Sleep(1000);
                }

                line.StoringPercent = 100;
                line.LineState = State.RunningStoring;
                dbo.ProductionLineModels.AddOrUpdate(line);
                dbo.SaveChanges();

                // Store Finished - Add to WHouse
                if (line.LineName.Contains("Marihuana"))
                {
                    string data = "ProductName=" + order.ProductName + "&Quantity=" + order.Amount +
                                  "&PricePerItem=0&SpaceRequiredPerItem=1&CreationDate=" + DateTime.Today +
                                  "&ProductCategory=Product";
                    HttpWebRequest httpRequest =
                        (HttpWebRequest)WebRequest.Create(storeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    var result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }
                    
                    data = "ProductName=Marihuana&Quantity=" + (order.Amount / 2) +
                            "&PricePerItem=0&SpaceRequiredPerItem=1&CreationDate=" + DateTime.Today +
                            "&ProductCategory=Product";
                    httpRequest =
                        (HttpWebRequest)WebRequest.Create(storeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                }
                if (line.LineName.Contains("Cocaine"))
                {
                    string data = "ProductName=" + order.ProductName + "&Quantity=" + order.Amount +
                                  "&PricePerItem=0&SpaceRequiredPerItem=1&CreationDate=" + DateTime.Today +
                                  "&ProductCategory=Product";
                    HttpWebRequest httpRequest =
                        (HttpWebRequest)WebRequest.Create(storeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    var result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }
                    
                    data = "ProductName=Cocaine&Quantity=" + (order.Amount / 2) +
                            "&PricePerItem=0&SpaceRequiredPerItem=1&CreationDate=" + DateTime.Today +
                            "&ProductCategory=Product";
                    httpRequest =
                        (HttpWebRequest)WebRequest.Create(storeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                }
                if (line.LineName.Contains("Meth"))
                {
                    string data = "ProductName=" + order.ProductName + "&Quantity=" + order.Amount +
                                  "&PricePerItem=0&SpaceRequiredPerItem=1&CreationDate=" + DateTime.Today +
                                  "&ProductCategory=Product";
                    HttpWebRequest httpRequest =
                        (HttpWebRequest)WebRequest.Create(storeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    var result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        line.LineState = State.Stopped;
                        line.CheckingQualityPercent = 0;
                        line.GetMaterialPercent = 0;
                        line.ProducingPercent = 0;
                        line.StoringPercent = 0;

                        dbo.ProductionLineModels.AddOrUpdate(line);
                        dbo.SaveChanges();
                    }
                    
                    data = "ProductName=Meth&Quantity=" + (order.Amount / 2) +
                            "&PricePerItem=0&SpaceRequiredPerItem=1&CreationDate=" + DateTime.Today +
                            "&ProductCategory=Product";
                    httpRequest =
                        (HttpWebRequest)WebRequest.Create(storeUrl);
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    result = (HttpWebResponse)httpRequest.GetResponse();
                }

                #endregion

                // Restart Process
                line.LineState = State.Stopped;
                line.CheckingQualityPercent = 0;
                line.GetMaterialPercent = 0;
                line.ProducingPercent = 0;
                line.StoringPercent = 0;

                dbo.ProductionLineModels.AddOrUpdate(line);
                dbo.SaveChanges();
            }

            using (var dbo = new ApplicationDbContext())
            {
                var line = dbo.ProductionLineModels.Find(lineName);
                if (line.Queue.Count > 0)
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(restartUrl);
                    httpRequest.Method = "GET";

                    var result = httpRequest.GetResponse();
                }
            }
        }

        public async Task<ActionResult> StartCocaine()
        {
            var lines = db.ProductionLineModels.Where(p => p.LineName.Contains("Cocaine"));
            if (lines == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            foreach (var line in lines)
            {
                if (line.Queue == null || line.Queue.Count == 0)
                {
                    line.LineState = State.Stopped;
                    line.CheckingQualityPercent = 0;
                    line.GetMaterialPercent = 0;
                    line.ProducingPercent = 0;
                    line.StoringPercent = 0;

                    db.ProductionLineModels.AddOrUpdate(line);
                    await db.SaveChangesAsync();
                }
                else
                {
                    BackgroundJob.Enqueue(() =>
                        RunLine(
                            line.LineName,
                            Url.Action("Take", "InputWarehouse", null, Request.Url.Scheme),
                            Url.Action("Store", "OutputWarehouse", null, Request.Url.Scheme),
                            Url.Action("StartCocaine", "ProductionLine", null, Request.Url.Scheme)));
                }
            }

            Thread.Sleep(110);
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> StartMeth()
        {
            var lines = db.ProductionLineModels.Where(p => p.LineName.Contains("Meth"));
            if (lines == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            foreach (var line in lines)
            {
                if (line.Queue == null || line.Queue.Count == 0)
                {
                    line.LineState = State.Stopped;
                    line.CheckingQualityPercent = 0;
                    line.GetMaterialPercent = 0;
                    line.ProducingPercent = 0;
                    line.StoringPercent = 0;

                    db.ProductionLineModels.AddOrUpdate(line);
                    await db.SaveChangesAsync();
                }
                else
                {
                    BackgroundJob.Enqueue(() =>
                        RunLine(
                            line.LineName,
                            Url.Action("Take", "InputWarehouse", null, Request.Url.Scheme),
                            Url.Action("Store", "OutputWarehouse", null, Request.Url.Scheme),
                            Url.Action("StartMeth", "ProductionLine", null, Request.Url.Scheme)));
                }
            }

            Thread.Sleep(110);
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
