﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;

namespace CartelloDiApis.Controllers
{
    public class PaycheckController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Paycheck/Details/5
        public ActionResult Details(int? id)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                PaycheckModel paycheckModel = db.PaycheckModels.Find(id);

                if (paycheckModel == null)
                {
                    return HttpNotFound();
                }

                return View(paycheckModel);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: Paycheck/PrintDetails/5
        public ActionResult PrintDetails(int? id)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                PaycheckModel paycheckModel = db.PaycheckModels.Find(id);

                if (paycheckModel == null)
                {
                    return HttpNotFound();
                }

                return View(paycheckModel);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
