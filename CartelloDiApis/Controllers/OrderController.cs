﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;
using System.Threading.Tasks;

namespace CartelloDiApis.Controllers
{
    public class OrderController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Order/Details/5
        public ActionResult Details(int? id)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Customer")))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                OrderModel orderModel = db.OrderModels.Find(id);

                if (orderModel == null)
                {
                    return HttpNotFound();
                }

                return View(orderModel);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        public async Task<ActionResult> Accept(int? id)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager")))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                OrderModel orderModel = db.OrderModels.Find(id);

                if (orderModel == null)
                {
                    return HttpNotFound();
                }

                orderModel.OrderStatus = Status.Accepted;
                await db.SaveChangesAsync();

                foreach (var item in orderModel.Items)
                {
                    var queue = new LineQueueModel();
                    queue.Amount = item.Count;
                    queue.ProductName = item.Name;

                    var url = "";

                    if (item.Name.Contains("Marihuana"))
                    {
                        url = Url.Action("StartMarihuana", "ProductionLine", null, Request.Url.Scheme);

                        var lines = db.ProductionLineModels.Where(p => p.LineName.Contains("Marihuana")).ToList();
                        var line = lines.Aggregate((l, r) => l.Queue.Count <= r.Queue.Count ? l : r);
                        queue.ProductionLineModelLineName = line.LineName;
                    }
                    if (item.Name.Contains("Cocaine"))
                    {
                        url = Url.Action("StartCocaine", "ProductionLine", null, Request.Url.Scheme);

                        var lines = db.ProductionLineModels.Where(p => p.LineName.Contains("Cocaine")).ToList();
                        var line = lines.Aggregate((l, r) => l.Queue.Count <= r.Queue.Count ? l : r);
                        queue.ProductionLineModelLineName = line.LineName;
                    }
                    if (item.Name.Contains("Meth"))
                    {
                        url = Url.Action("StartMeth", "ProductionLine", null, Request.Url.Scheme);

                        var lines = db.ProductionLineModels.Where(p => p.LineName.Contains("Meth")).ToList();
                        var line = lines.Aggregate((l, r) => l.Queue.Count <= r.Queue.Count ? l : r);
                        queue.ProductionLineModelLineName = line.LineName;
                    }

                    db.LineQueueModels.Add(queue);
                    await db.SaveChangesAsync();

                    HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "GET";

                    var result = httpRequest.GetResponse();

                }

                return RedirectToAction("Manage", "Order");
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }


        public async Task<ActionResult> Decline(int? id)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager")))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                OrderModel orderModel = db.OrderModels.Find(id);

                if (orderModel == null)
                {
                    return HttpNotFound();
                }

                orderModel.OrderStatus = Status.Declined;
                await db.SaveChangesAsync();

                return RedirectToAction("Manage", "Order");
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: Order/PrintDetails/5
        public ActionResult PrintDetails(int? id)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Customer")))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                OrderModel orderModel = db.OrderModels.Find(id);

                if (orderModel == null)
                {
                    return HttpNotFound();
                }

                return View(orderModel);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }


        // GET: Order/Create
        public ActionResult Create()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Customer")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                return View();
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // POST: Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CustomerAddress1,CustomerAddress2,CustomerPostalCode,CustomerName,Companyddress1,CompanyAddress2,CompanyPostalCode,CompanyName")] OrderModel orderModel, int mari,int coc, int meth, int mari2, int coc2, int meth2)
        {

            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Customer")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                if (ModelState.IsValid)
                {
                    orderModel.Email = User.Identity.Name;
                    orderModel.Companyddress1 = "795 Folsom Ave, Suite 600";
                    orderModel.CompanyAddress2 = "San Francisco, CA";
                    orderModel.CompanyPostalCode = "94107";
                    orderModel.CompanyName = "Cartello Di Apis Inc.";
                    orderModel.Date = DateTime.Now;
                    orderModel.OrderStatus = Status.New;
                    db.OrderModels.Add(orderModel);
                    db.SaveChanges();

                    if (mari > 0)
                    {
                        ItemModel pureMarihuana = new ItemModel();
                        pureMarihuana.Count = mari;
                        pureMarihuana.Name = "PureMarihuana";
                        pureMarihuana.PricePerUnit = 17000;
                        pureMarihuana.OrderModelID = orderModel.ID;
                        db.ItemModels.Add(pureMarihuana);
                        db.SaveChanges();
                    }

                    if (mari2 > 0)
                    {
                        ItemModel marihuana = new ItemModel();
                        marihuana.Count = mari2;
                        marihuana.Name = "Marihuana";
                        marihuana.PricePerUnit = 10000;
                        marihuana.OrderModelID = orderModel.ID;
                        db.ItemModels.Add(marihuana);
                        db.SaveChanges();
                    }

                    if (coc > 0)
                    {
                        ItemModel PureCocaine = new ItemModel();
                        PureCocaine.Count = coc;
                        PureCocaine.Name = "PureCocaine";
                        PureCocaine.PricePerUnit = 120000;
                        PureCocaine.OrderModelID = orderModel.ID;
                        db.ItemModels.Add(PureCocaine);
                        db.SaveChanges();
                    }

                    if (coc2 > 0)
                    {
                        ItemModel cocaine = new ItemModel();
                        cocaine.Count = coc2;
                        cocaine.Name = "Cocaine";
                        cocaine.PricePerUnit = 80000;
                        cocaine.OrderModelID = orderModel.ID;
                        db.ItemModels.Add(cocaine);
                        db.SaveChanges();
                    }

                    if (meth > 0)
                    {
                        ItemModel PureMeth = new ItemModel();
                        PureMeth.Count = meth;
                        PureMeth.Name = "PureMeth";
                        PureMeth.PricePerUnit = 100000;
                        PureMeth.OrderModelID = orderModel.ID;
                        db.ItemModels.Add(PureMeth);
                        db.SaveChanges();
                    }

                    if (meth2 > 0)
                    {
                        ItemModel Meth = new ItemModel();
                        Meth.Count = meth2;
                        Meth.Name = "Meth";
                        Meth.PricePerUnit = 70000;
                        Meth.OrderModelID = orderModel.ID;
                        db.ItemModels.Add(Meth);
                        db.SaveChanges();
                    }


                    if (User.IsInRole("Manager")){
                        return RedirectToAction("Manage");
                    }
                    if (User.IsInRole("Customer")){
                        return RedirectToAction("MyOrders");
                    }

                }

                return View(orderModel);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

      
        // GET: Order/Manage/5
        public ActionResult Manage()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Manager"))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                ViewBag.UserEmail = User.Identity.Name;

                var orders = db.OrderModels.ToList();
                var orderViews = new List<OrderViewModel>();

                foreach (var order in orders)
                {
                    var orderView = new OrderViewModel();
                    orderView.ID = order.ID;
                    orderView.CustomerName = order.CustomerName;
                    orderView.CustomerAddress1 = order.CustomerAddress1;
                    orderView.CustomerAddress2 = order.CustomerAddress2;
                    orderView.CustomerPostalCode = order.CustomerPostalCode;
                    orderView.Date = order.Date;
                    orderView.OrderStatus = order.OrderStatus;

                    bool finishable = true;

                    foreach (var item in order.Items)
                    {
                        var itemInDb = db.OutputWarehouseModels.Where(p => p.ProductName == item.Name).ToList();
                        if (itemInDb.Count <= 0)
                        {
                            finishable = false;
                            break;
                        }

                        if(itemInDb[0].Quantity < item.Count)
                        {
                            finishable = false;
                            break;
                        }
                    }

                    orderView.Finishable = finishable;
                    orderViews.Add(orderView);
                }

                return View(orderViews);
            }           
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        public ActionResult Finish(int? id)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Manager"))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                ViewBag.UserEmail = User.Identity.Name;

                var order = db.OrderModels.Find(id);
                order.OrderStatus = Status.Finished;
                db.OrderModels.AddOrUpdate(order);
                db.SaveChanges();

                double total = 0;
                foreach (var item in order.Items)
                {
                    total += item.Count * item.PricePerUnit;

                    string data = "ProductName=" + item.Name + "&Amount=" + item.Count;
                    HttpWebRequest httpRequest = 
                        (HttpWebRequest)WebRequest.Create(Url.Action("Sell", "OutputWarehouse", null, Request.Url.Scheme));
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.ContentLength = data.Length;

                    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                    streamWriter.Write(data);
                    streamWriter.Close();

                    var result = (HttpWebResponse)httpRequest.GetResponse();
                    if (result.StatusCode != HttpStatusCode.OK)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                }

                var trans = new TransactionModel();
                trans.AccountantModelAcountantName = "DefaultAccountant";
                trans.Amount = total;
                trans.Date = DateTime.Now;
                trans.Reason = "Finished Order #" + order.ID;
                db.TransactionModels.Add(trans);
                db.SaveChanges();

                var acc = db.AccountantModels.Find("DefaultAccountant");
                acc.SafeMoney += trans.Amount;
                db.AccountantModels.AddOrUpdate(acc);
                db.SaveChanges();

                return RedirectToAction("Manage");
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: Order/MyOrders/5
        public ActionResult MyOrders()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Customer"))
            {
                ViewBag.UserRole = "Customer";

                var orders = from o  in db.OrderModels
                             where o.Email == User.Identity.Name
                             select o;
                return View(orders.ToList());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
