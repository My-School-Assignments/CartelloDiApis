﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;
using Microsoft.AspNet.Identity.Owin;

namespace CartelloDiApis.Controllers
{
    public class EmployeeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Employee
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Manager"))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                ViewBag.UserEmail = User.Identity.Name;
                return View(db.EmployeeModels.ToList());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                EmployeeModel employeeModel = db.EmployeeModels.Find(id);

                if (employeeModel == null)
                {
                    return HttpNotFound();
                }

                if (User.IsInRole("Employee") && !User.Identity.Name.Equals(employeeModel.Email))
                {
                    employeeModel = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name);

                    return View(employeeModel);
                }

                return View(employeeModel);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                return View();
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,Email,Address1,Address2,PostalCode,HourlyPay,HoursThisPeriod,BitcoinAddress")] EmployeeModel employeeModel)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Manager"))
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser {UserName = employeeModel.Email, Email = employeeModel.Email};
                    var result = await UserManager.CreateAsync(user, "Default.123");
                    if (result.Succeeded)
                    {
                        await this.UserManager.AddToRoleAsync(user.Id, "Employee");
                        employeeModel.HoursThisPeriod = 0;
                        db.EmployeeModels.Add(employeeModel);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }

                return View(employeeModel);
            }

            return RedirectToAction("LogIn", "Account");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddHours(FormCollection formObj)
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Employee") || User.IsInRole("Manager")))
            {
                var emp = db.EmployeeModels.Find(Int32.Parse(formObj["empID"]));
                if (emp != null)
                {
                    emp.HoursThisPeriod += Double.Parse(formObj["hours"]);
                    db.EmployeeModels.AddOrUpdate(emp);
                    await db.SaveChangesAsync();
                }

                return RedirectToAction("Details", "Employee", new { id = Int32.Parse(formObj["empID"]) });
            }

            return RedirectToAction("LogIn", "Account");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPay(FormCollection formObj)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Manager"))
            {
                var emp = db.EmployeeModels.Find(Int32.Parse(formObj["empID"]));
                if (emp != null)
                {
                    emp.HourlyPay = Double.Parse(formObj["newPay"]);
                    db.EmployeeModels.AddOrUpdate(emp);
                    await db.SaveChangesAsync();
                }

                return RedirectToAction("Details", "Employee", new {id = Int32.Parse(formObj["empID"])});
            }

            return RedirectToAction("LogIn", "Account");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
