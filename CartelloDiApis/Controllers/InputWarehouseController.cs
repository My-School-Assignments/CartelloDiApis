﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;

namespace CartelloDiApis.Controllers
{
    public class InputWarehouseController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: InputWarehouse
        public async Task<ActionResult> Index()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                return View(await db.InputWarehouseModels.Where(p => p.WarehouseModelName == "InputWHouse1").ToListAsync());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // GET: InputWarehouse/Accept
        public ActionResult Accept()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                return View();
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // POST: InputWarehouse/Accept
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Accept([Bind(Include = "ID,ProductName,Quantity,PricePerItem,SpaceRequiredPerItem,WarehouseAcceptanceDate,ProductCategory")] InputWarehouseModel inputWarehouseModel)
        {
            if (ModelState.IsValid)
            {
                InputWarehouseModel item = db.InputWarehouseModels.FirstOrDefault(
                           p => p.ProductName == inputWarehouseModel.ProductName);

                if (item == null)
                {
                    WarehouseModel whouse = db.WarehouseModels.Find("InputWHouse1");
                    if ((whouse.FreeCapacity -
                         (inputWarehouseModel.Quantity * inputWarehouseModel.SpaceRequiredPerItem)) >= 0)
                    {
                        whouse.FreeCapacity -=
                            inputWarehouseModel.Quantity * inputWarehouseModel.SpaceRequiredPerItem;
                        db.WarehouseModels.AddOrUpdate(whouse);
                        await db.SaveChangesAsync();

                        inputWarehouseModel.WarehouseModelName = "InputWHouse1";
                        inputWarehouseModel.WarehouseAcceptanceDate = DateTime.Today;
                        db.InputWarehouseModels.Add(inputWarehouseModel);
                        await db.SaveChangesAsync();

                        var acc = db.AccountantModels.Find("DefaultAccountant");

                        var trans = new TransactionModel();
                        trans.AccountantModelAcountantName = "DefaultAccountant";
                        trans.Amount = -inputWarehouseModel.Quantity * inputWarehouseModel.PricePerItem;
                        trans.Date = DateTime.Today;
                        trans.Reason = "Accepted item: " + inputWarehouseModel.ProductName + " Ammount: " + inputWarehouseModel.Quantity;
                        db.TransactionModels.Add(trans);
                        await db.SaveChangesAsync();

                        acc.SafeMoney += trans.Amount;
                        db.AccountantModels.AddOrUpdate(acc);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        return RedirectToAction("Accept");
                    }
                }
                else
                {
                    WarehouseModel whouse = db.WarehouseModels.Find("InputWHouse1");
                    if ((whouse.FreeCapacity -
                         (inputWarehouseModel.Quantity * inputWarehouseModel.SpaceRequiredPerItem)) >= 0)
                    {
                        whouse.FreeCapacity -=
                            inputWarehouseModel.Quantity * inputWarehouseModel.SpaceRequiredPerItem;
                        db.WarehouseModels.AddOrUpdate(whouse);
                        await db.SaveChangesAsync();

                        item.Quantity += inputWarehouseModel.Quantity;
                        item.WarehouseAcceptanceDate = DateTime.Today;
                        db.InputWarehouseModels.AddOrUpdate(item);
                        await db.SaveChangesAsync();

                        var acc = db.AccountantModels.Find("DefaultAccountant");

                        var trans = new TransactionModel();
                        trans.AccountantModelAcountantName = "DefaultAccountant";
                        trans.Amount = -item.Quantity * item.PricePerItem;
                        trans.Date = DateTime.Today;
                        trans.Reason = "Accepted item: " + item.ProductName + " Ammount: " + item.Quantity;
                        db.TransactionModels.Add(trans);
                        await db.SaveChangesAsync();

                        acc.SafeMoney += trans.Amount;
                        db.AccountantModels.AddOrUpdate(acc);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        return RedirectToAction("Accept");
                    }
                }
                
                return RedirectToAction("Index");
            }

            return RedirectToAction("Accept");
        }

        // POST: InputWarehouse/Take
        [HttpPost]
        public async Task<ActionResult> Take([Bind(Include = "ProductName,Amount")] SellModel sellModel)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            InputWarehouseModel item = db.InputWarehouseModels.FirstOrDefault(p => p.ProductName == sellModel.ProductName);
            if (item == null || item.Quantity < sellModel.Amount)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }

            WarehouseModel whouse = db.WarehouseModels.Find(item.WarehouseModelName);

            if (item.Quantity - sellModel.Amount == 0)
            {
                db.InputWarehouseModels.Remove(item);
                await db.SaveChangesAsync();

                whouse.FreeCapacity += sellModel.Amount;
                await db.SaveChangesAsync();

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            item.Quantity -= sellModel.Amount;
            db.InputWarehouseModels.AddOrUpdate(item);
            await db.SaveChangesAsync();

            whouse.FreeCapacity += sellModel.Amount;
            await db.SaveChangesAsync();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
