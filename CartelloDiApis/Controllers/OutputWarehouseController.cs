﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartelloDiApis.Models;

namespace CartelloDiApis.Controllers
{
    public class OutputWarehouseController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: OutputWarehouse
        public async Task<ActionResult> Index()
        {
            if (User.Identity.IsAuthenticated && (User.IsInRole("Manager") || User.IsInRole("Employee")))
            {
                if (User.IsInRole("Manager")) ViewBag.UserRole = "Manager";
                if (User.IsInRole("Customer")) ViewBag.UserRole = "Customer";
                if (User.IsInRole("Employee")) ViewBag.UserRole = "Employee";
                ViewBag.UserEmail = User.Identity.Name;
                ViewBag.UserID = db.EmployeeModels.FirstOrDefault(m => m.Email == User.Identity.Name)?.ID;

                return View(await db.OutputWarehouseModels.Where(p => p.WarehouseModelName == "OutputWHouse1").ToListAsync());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        // POST: OutputWarehouse/Store
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<ActionResult> Store([Bind(Include = "ProductName,Quantity,PricePerItem,SpaceRequiredPerItem,CreationDate,ProductCategory")] OutputWarehouseModel outputWarehouseModel)
        {
            if (ModelState.IsValid)
            {
                OutputWarehouseModel item = db.OutputWarehouseModels.FirstOrDefault(
                        p => p.ProductName == outputWarehouseModel.ProductName);

                if (item == null)
                {
                    WarehouseModel whouse = db.WarehouseModels.Find("OutputWHouse1");
                    if ((whouse.FreeCapacity -
                         (outputWarehouseModel.Quantity * outputWarehouseModel.SpaceRequiredPerItem)) >= 0)
                    {
                        whouse.FreeCapacity -=
                            outputWarehouseModel.Quantity * outputWarehouseModel.SpaceRequiredPerItem;
                        db.WarehouseModels.AddOrUpdate(whouse);
                        await db.SaveChangesAsync();

                        outputWarehouseModel.WarehouseModelName = "OutputWHouse1";
                        db.OutputWarehouseModels.Add(outputWarehouseModel);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.Conflict);
                    }
                }
                else
                {
                    WarehouseModel whouse = db.WarehouseModels.Find("OutputWHouse1");
                    if ((whouse.FreeCapacity -
                         (outputWarehouseModel.Quantity * outputWarehouseModel.SpaceRequiredPerItem)) >= 0)
                    {
                        whouse.FreeCapacity -=
                            outputWarehouseModel.Quantity * outputWarehouseModel.SpaceRequiredPerItem;
                        db.WarehouseModels.AddOrUpdate(whouse);
                        await db.SaveChangesAsync();

                        item.Quantity += outputWarehouseModel.Quantity;
                        db.OutputWarehouseModels.AddOrUpdate(item);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.Conflict);
                    }
                }

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: OutputWarehouse/GetAmmount
        [HttpPost]
        public ActionResult GetAmmount([Bind(Include = "ProductName,Amount")] SellModel sellModel)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OutputWarehouseModel item = db.OutputWarehouseModels.FirstOrDefault(p => p.ProductName == sellModel.ProductName);
            if (item == null || item.Quantity < sellModel.Amount)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        // POST: OutputWarehouse/Sell
        [HttpPost]
        public async Task<ActionResult> Sell([Bind(Include = "ProductName,Amount")] SellModel sellModel)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OutputWarehouseModel item = db.OutputWarehouseModels.FirstOrDefault(p => p.ProductName == sellModel.ProductName);
            if (item == null || item.Quantity < sellModel.Amount)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }

            WarehouseModel whouse = db.WarehouseModels.Find(item.WarehouseModelName);
            var acc = db.AccountantModels.Find("DefaultAccountant");

            if (item.Quantity - sellModel.Amount == 0)
            {
                db.OutputWarehouseModels.Remove(item);
                await db.SaveChangesAsync();

                whouse.FreeCapacity += sellModel.Amount;
                await db.SaveChangesAsync();

                var trans1 = new TransactionModel();
                trans1.AccountantModelAcountantName = "DefaultAccountant";
                trans1.Amount = item.Quantity * item.PricePerItem;
                trans1.Date = DateTime.Today;
                trans1.Reason = "Sold item: " + item.ProductName + " Ammount: " + item.Quantity;
                db.TransactionModels.Add(trans1);
                await db.SaveChangesAsync();

                acc.SafeMoney += trans1.Amount;
                db.AccountantModels.AddOrUpdate(acc);
                await db.SaveChangesAsync();

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            item.Quantity -= sellModel.Amount;
            db.OutputWarehouseModels.AddOrUpdate(item);
            await db.SaveChangesAsync();

            whouse.FreeCapacity += sellModel.Amount;
            await db.SaveChangesAsync();

            var trans = new TransactionModel();
            trans.AccountantModelAcountantName = "DefaultAccountant";
            trans.Amount = item.Quantity * item.PricePerItem;
            trans.Date = DateTime.Today;
            trans.Reason = "Sold item: " + item.ProductName + " Ammount: " + item.Quantity;
            db.TransactionModels.Add(trans);
            await db.SaveChangesAsync();

            acc.SafeMoney += trans.Amount;
            db.AccountantModels.AddOrUpdate(acc);
            await db.SaveChangesAsync();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
