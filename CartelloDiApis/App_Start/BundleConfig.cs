﻿using System.Web;
using System.Web.Optimization;

namespace CartelloDiApis
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/FormsCss").Include(
                "~/Content/bootstrap/css/bootstrap.min.css", new CssRewriteUrlTransform()).Include(
                "~/Content/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform()).Include(
                "~/Content/css/local.css", new CssRewriteUrlTransform()
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate.js"
                ));

            bundles.Add(new StyleBundle("~/bundles/RestCss").Include(
                "~/Content/bootstrap/css/bootstrap.min.css", new CssRewriteUrlTransform()).Include(
                "~/Content/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform()).Include(
                "~/Content/css/local.css", new CssRewriteUrlTransform()//).Include(
                //"~/Content/light-bootstrap/all.min.css", new CssRewriteUrlTransform()).Include(
                //"~/Content/dark-bootstrap/all.min.css", new CssRewriteUrlTransform()
                ));

            bundles.Add(new ScriptBundle("~/bundles/FormsJs").Include(
                "~/Content/js/jquery-1.10.2.min.js",
                "~/Content/bootstrap/js/bootstrap.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/RestJs").Include(
                "~/Content/js/jquery-1.10.2.min.js",
                "~/Content/bootstrap/js/bootstrap.min.js"//,
                //"~/Content/shieldui-all.min.js",
                //"~/Content/gridData.js"
                ));
        }
    }
}
